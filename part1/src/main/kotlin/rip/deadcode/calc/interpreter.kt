package rip.deadcode.calc

import rip.deadcode.calc.Expression.NumberExpression
import rip.deadcode.calc.Expression.OperativeExpression
import rip.deadcode.calc.Token.Operator.*

fun interpret(expression: Expression): Int {

    return when (expression) {
        is NumberExpression -> {
            expression.number.token.toInt()
        }
        is OperativeExpression -> {

            val arguments = expression.arguments.map { interpret(it) }

            when (expression.operator) {
                is Plus -> arguments.sum()
                is Minus -> arguments.reduce { acc, i -> acc - i }
                is Multiply -> arguments.reduce { acc, i -> acc * i }
                is Divide -> arguments.reduce { acc, i -> acc / i }
            }
        }
    }
}
