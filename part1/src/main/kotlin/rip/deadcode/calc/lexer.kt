package rip.deadcode.calc

import rip.deadcode.calc.Token.Braces.ParenthesesClose
import rip.deadcode.calc.Token.Braces.ParenthesesOpen
import rip.deadcode.calc.Token.Operator.*

data class LexResult(
        val token: Token,
        val rest: String
)

fun lex(source: String): List<Token> {
    val result = lexSymbols(source)
            ?: lexNumber(source)
            ?: lexWhitespace(source)
            ?: throw RuntimeException("Unexpected token near: " + source.substring(0, 10))

    val token = if (result.token != Token.Whitespace) {
        listOf(result.token)
    } else {
        listOf()
    }

    return if (result.rest.isEmpty()) {
        token
    } else {
        token + lex(result.rest)
    }
}

fun lexSymbols(source: String): LexResult? {
    return when {
        source.startsWith("+") -> LexResult(Plus, source.substring(1))
        source.startsWith("-") -> LexResult(Minus, source.substring(1))
        source.startsWith("*") -> LexResult(Multiply, source.substring(1))
        source.startsWith("/") -> LexResult(Divide, source.substring(1))
        source.startsWith("(") -> LexResult(ParenthesesOpen, source.substring(1))
        source.startsWith(")") -> LexResult(ParenthesesClose, source.substring(1))
        else -> null
    }
}

private val regNumber = Regex("^[1-9][0-9]*")

fun lexNumber(source: String): LexResult? {
    val result = regNumber.find(source)
    return if (result != null) {
        LexResult(Token.Number(result.value), source.substring(result.range.last + 1))
    } else {
        null
    }
}

fun lexWhitespace(source: String): LexResult? {
    return if (source[0].isWhitespace() || source[0] == '\r' || source[0] == '\n') {
        LexResult(Token.Whitespace, source.substring(1))
    } else {
        null
    }
}
