package rip.deadcode.calc

import rip.deadcode.calc.Expression.NumberExpression
import rip.deadcode.calc.Expression.OperativeExpression
import rip.deadcode.calc.Token.Braces.ParenthesesClose
import rip.deadcode.calc.Token.Braces.ParenthesesOpen
import rip.deadcode.calc.Token.Operator

data class ParseResult<out T>(
        val result: T,
        val unparsed: List<Token>
)

fun parseExpression(tokens: List<Token>): ParseResult<Expression> {

    return if (tokens.isNotEmpty() && tokens[0] == ParenthesesOpen) {
        val op = tokens[1] as Operator
        val arguments = parseArguments(tokens.skip(2))
        if (arguments.unparsed[0] != ParenthesesClose) throw RuntimeException("Unexpected token: " + arguments.unparsed[0])

        ParseResult(OperativeExpression(op, arguments.result), arguments.unparsed.skip(1))

    } else {
        ParseResult(NumberExpression(tokens[0] as Token.Number), tokens.skip(1))
    }
}

fun parseArguments(tokens: List<Token>): ParseResult<List<Expression>> {

    return if (tokens[0] != ParenthesesClose) {
        val expr = parseExpression(tokens)
        val trailing = parseArguments(expr.unparsed)
        ParseResult(listOf(expr.result) + trailing.result, trailing.unparsed)

    } else {
        ParseResult(listOf(), tokens)
    }
}
