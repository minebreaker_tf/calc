package rip.deadcode.calc

import rip.deadcode.calc.Token.Number
import rip.deadcode.calc.Token.Operator

sealed class Expression {

    data class OperativeExpression(
            val operator: Operator,
            val arguments: List<Expression>
    ) : Expression()

    data class NumberExpression(
            val number: Number
    ) : Expression()
}
