package rip.deadcode.calc

fun <T> List<T>.skip(from: Int): List<T> =
        if (from == this.size) listOf() else this.subList(from, this.size)
