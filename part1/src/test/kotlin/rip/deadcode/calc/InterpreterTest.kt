package rip.deadcode.calc

import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.Test

class InterpreterTest {

    @Test
    fun test1() {
        val result = interpret(parseExpression(lex("(+ 1 2)")).result)
        assertThat(result).isEqualTo(3)
    }

    @Test
    fun test2() {
        val result = interpret(parseExpression(lex("(- (/ 12 2) (* 2 (+ 1 2)))")).result)
        assertThat(result).isEqualTo(0)
    }

}
