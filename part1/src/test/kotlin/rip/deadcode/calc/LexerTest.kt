package rip.deadcode.calc

import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.Test

internal class LexerTest {

    @Test
    fun testLex1() {
        val result =lex("(+ 1 2 3)")
        assertThat(result[0].token).isEqualTo("(")
        assertThat(result[1].token).isEqualTo("+")
        assertThat(result[2].token).isEqualTo("1")
        assertThat(result[3].token).isEqualTo("2")
        assertThat(result[4].token).isEqualTo("3")
        assertThat(result[5].token).isEqualTo(")")
    }

    @Test
    fun testLex2() {
        val result =lex("(- (/ 8 2) (* 2 2)")
        assertThat(result[0].token).isEqualTo("(")
        assertThat(result[1].token).isEqualTo("-")
        assertThat(result[2].token).isEqualTo("(")
        assertThat(result[3].token).isEqualTo("/")
        assertThat(result[4].token).isEqualTo("8")
        assertThat(result[5].token).isEqualTo("2")
        assertThat(result[6].token).isEqualTo(")")
        assertThat(result[7].token).isEqualTo("(")
        assertThat(result[8].token).isEqualTo("*")
        assertThat(result[9].token).isEqualTo("2")
        assertThat(result[10].token).isEqualTo("2")
        assertThat(result[11].token).isEqualTo(")")
    }
}
