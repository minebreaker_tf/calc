package rip.deadcode.calc

import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.Test
import rip.deadcode.calc.Expression.NumberExpression
import rip.deadcode.calc.Expression.OperativeExpression
import rip.deadcode.calc.Token.Operator.*

class ParserTest {

    @Test
    fun testParse1() {
        val result = parseExpression(lex("(+ 1 2)"))
        val expr = result.result as OperativeExpression
        assertThat(result.unparsed).isEmpty()
        assertThat(expr.operator).isEqualTo(Plus)
        assertThat((expr.arguments[0] as NumberExpression).number.token).isEqualTo("1")
        assertThat((expr.arguments[1] as NumberExpression).number.token).isEqualTo("2")
    }

    @Test
    fun testParse2() {
        val result = parseExpression(lex("(- (/ 12 2) (* 2 3))"))
        val expr = result.result as OperativeExpression
        val expr1 = expr.arguments[0] as OperativeExpression
        val expr2 = expr.arguments[1] as OperativeExpression

        assertThat(result.unparsed).isEmpty()
        assertThat(expr.operator).isEqualTo(Minus)
        assertThat(expr1.operator).isEqualTo(Divide)
        assertThat((expr1.arguments[0] as NumberExpression).number.token).isEqualTo("12")
        assertThat((expr1.arguments[1] as NumberExpression).number.token).isEqualTo("2")
        assertThat(expr2.operator).isEqualTo(Multiply)
        assertThat((expr2.arguments[0] as NumberExpression).number.token).isEqualTo("2")
        assertThat((expr2.arguments[1] as NumberExpression).number.token).isEqualTo("3")
    }
}
