expression: '(' OPERATOR (expression)+ ')'
          | NUMBER
          ;

OPERATOR: '+'
        | '-'
        | '*'
        | '/'
        ;

NUMBER: [1-9][0-9]*
      ;
