package rip.deadcode.calc

fun interpretExpression(expression: Expression): Int {

    return if (expression.rest == null) {
        interpretSecondaryExpression(expression.expression)
    } else {

        val left = interpretSecondaryExpression(expression.expression)
        val op = expression.rest.operator
        val right = interpretSecondaryExpression(expression.rest.secondaryExpression)

        val result = when (op) {
            Token.Operator.Plus -> left + right
            Token.Operator.Minus -> left - right
        }

        if (expression.rest.rest != null) {
            interpretExpression(
                    Expression(SecondaryExpression(Token.Number(result.toString()), null), expression.rest.rest))
        } else {
            result
        }
    }
}

fun interpretSecondaryExpression(secondaryExpression: SecondaryExpression): Int {

    return if (secondaryExpression.secondaryRest == null) {
        secondaryExpression.number.token.toInt()
    } else {

        val left = secondaryExpression.number.token.toInt()
        val op = secondaryExpression.secondaryRest.secondaryOperator
        val right = secondaryExpression.secondaryRest.number.token.toInt()

        val result = when (op) {
            Token.SecondaryOperator.Multiply -> left * right
            Token.SecondaryOperator.Divide -> left / right
        }

        if (secondaryExpression.secondaryRest.secondaryRest != null) {
            interpretSecondaryExpression(SecondaryExpression(
                    Token.Number(result.toString()), secondaryExpression.secondaryRest.secondaryRest))
        } else {
            result
        }
    }
}
