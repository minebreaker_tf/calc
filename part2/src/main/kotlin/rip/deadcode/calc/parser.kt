package rip.deadcode.calc

data class ParseResult<T>(
        val result: T,
        val unparsed: List<Token>
)

fun parseExpression(tokens: List<Token>): ParseResult<Expression> {

    val secondaryExpression = parseSecondaryExpression(tokens)
    val rest = parseRest(secondaryExpression.unparsed)

    return if (rest != null) {
        ParseResult(Expression(secondaryExpression.result, rest.result), rest.unparsed)
    } else {
        ParseResult(Expression(secondaryExpression.result, null), secondaryExpression.unparsed)
    }
}

fun parseRest(tokens: List<Token>): ParseResult<Rest>? {

    return if (tokens.isNotEmpty() && tokens[0] is Token.Operator) {

        val operator = tokens[0] as Token.Operator
        val secondaryExpression = parseSecondaryExpression(tokens.subList(1, tokens.size))
        val rest = parseRest(secondaryExpression.unparsed)

        return if (rest != null) {
            ParseResult(Rest(operator, secondaryExpression.result, rest.result), rest.unparsed)
        } else {
            ParseResult(Rest(operator, secondaryExpression.result, null), secondaryExpression.unparsed)
        }

    } else {
        null
    }
}

fun parseSecondaryExpression(tokens: List<Token>): ParseResult<SecondaryExpression> {

    val number = tokens[0] as Token.Number
    val secondaryRest = parseSecondaryRest(tokens.subList(1, tokens.size))

    return if (secondaryRest != null) {
        ParseResult(SecondaryExpression(number, secondaryRest.result), secondaryRest.unparsed)
    } else {
        val unparsed = if (tokens.size > 1) tokens.subList(1, tokens.size) else listOf()
        ParseResult(SecondaryExpression(number, null), unparsed)
    }
}

fun parseSecondaryRest(tokens: List<Token>): ParseResult<SecondaryRest>? {

    return if (tokens.isNotEmpty() && tokens[0] is Token.SecondaryOperator) {

        val secondaryOperator = tokens[0] as Token.SecondaryOperator
        val number = tokens[1] as Token.Number
        val secondaryRest = parseSecondaryRest(tokens.subList(2, tokens.size))

        return if (secondaryRest != null) {
            ParseResult(SecondaryRest(secondaryOperator, number, secondaryRest.result), secondaryRest.unparsed)
        } else {
            val unparsed = if (tokens.size > 2) tokens.subList(2, tokens.size) else listOf()
            ParseResult(SecondaryRest(secondaryOperator, number, null), unparsed)
        }

    } else {
        null
    }
}
