package rip.deadcode.calc

data class Expression(
        val expression: SecondaryExpression,
        val rest: Rest?
)

data class Rest(
        val operator: Token.Operator,
        val secondaryExpression: SecondaryExpression,
        val rest: Rest?
)

data class SecondaryExpression(
        val number: Token.Number,
        val secondaryRest: SecondaryRest?
)

data class SecondaryRest(
        val secondaryOperator: Token.SecondaryOperator,
        val number: Token.Number,
        val secondaryRest: SecondaryRest?
)
