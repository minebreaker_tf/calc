package rip.deadcode.calc

import com.google.common.base.MoreObjects
import java.util.*

sealed class Token(val token: String) {

    sealed class Operator(token: String) : Token(token) {
        object Plus : Operator("+")
        object Minus : Operator("-")
    }

    sealed class SecondaryOperator(token: String) : Token(token) {
        object Multiply : SecondaryOperator("*")
        object Divide : SecondaryOperator("/")
    }

    class Number(number: String) : Token(number)

    object Whitespace : Token(" ")

    override fun equals(other: Any?): Boolean {
        return other is Token && this::class == other::class && this.token == other.token
    }

    override fun hashCode(): Int {
        return Objects.hashCode(token)
    }

    override fun toString(): String {
        return MoreObjects.toStringHelper(this).add("token", token).toString()
    }
}
