package rip.deadcode.calc

import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.Test

class InterpreterTest {

    @Test
    fun testInterpret1() {
        val source = "1 + 2 * 3"
        val result = interpretExpression(parseExpression(lex(source)).result)
        assertThat(result).isEqualTo(7)
    }

    @Test
    fun testInterpret2() {
        val source = "1 * 2 + 3"
        val result = interpretExpression(parseExpression(lex(source)).result)
        assertThat(result).isEqualTo(5)
    }

    @Test
    fun testInterpret3() {
        val source = "11 - 4 / 2 + 1"
        val result = interpretExpression(parseExpression(lex(source)).result)
        assertThat(result).isEqualTo(10)
    }

    @Test
    fun testInterpret4() {
        val source = "4-3-2-1"
        val result = interpretExpression(parseExpression(lex(source)).result)
        assertThat(result).isEqualTo(-2)
    }

    @Test
    fun testInterpret5() {
        val source = "100 / 4 - 20 / 5 - 1"
        val result = interpretExpression(parseExpression(lex(source)).result)
        assertThat(result).isEqualTo(20)
    }
}
