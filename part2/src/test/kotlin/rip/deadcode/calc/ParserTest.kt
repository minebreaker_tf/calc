package rip.deadcode.calc

import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.Test

class ParserTest {

    @Test
    fun testParseExpression1() {

        val result = parseExpression(lex("123"))

        println(result)

        val resultExpr = result.result
        val left = resultExpr.expression
        assertThat(result.unparsed).isEmpty()
        assertThat(left.number.token).isEqualTo("123")
        assertThat(resultExpr.rest).isNull()
    }

    @Test
    fun testParseExpression2() {

        val result = parseExpression(lex("123 - 456"))

        println(result)

        val resultExpr = result.result
        val left = resultExpr.expression
        val right = resultExpr.rest!!.secondaryExpression
        val op = resultExpr.rest!!.operator
        assertThat(result.unparsed).isEmpty()
        assertThat(left.number.token).isEqualTo("123")
        assertThat(op).isEqualTo(Token.Operator.Minus)
        assertThat(right.number.token).isEqualTo("456")
        assertThat(right.secondaryRest).isNull()
    }

    @Test
    fun testParseExpression3() {
        val result = parseExpression(lex("1*2-3"))
        println(result)
    }

    @Test
    fun testParseExpression4() {
        val result = parseExpression(lex("1+2/3"))
        println(result)
    }
}
