package rip.deadcode.calc

import rip.deadcode.calc.Expression.*
import rip.deadcode.calc.Expression.Function
import rip.deadcode.calc.Token.Operator.PrimaryOperator.Minus
import rip.deadcode.calc.Token.Operator.PrimaryOperator.Plus
import rip.deadcode.calc.Token.Operator.SecondaryOperator.Divide
import rip.deadcode.calc.Token.Operator.SecondaryOperator.Multiply

fun interpret(expression: Expression): Int {
    return when (expression) {
        is OperativeExpression -> interpretExpression(expression)
        is Function -> interpretFunction(expression)
        is NumberTerm -> interpretNumberTerm(expression)
    }
}

private fun interpretExpression(expression: OperativeExpression): Int {
    val left = interpret(expression.left)
    val right = interpret(expression.right)

    return when (expression.operator) {
        Plus -> left + right
        Minus -> left - right
        Multiply -> left * right
        Divide -> left / right
    }
}

private val functions = mapOf(
        "sqrt" to { args: IntArray -> Math.sqrt(args[0].toDouble()).toInt() },
        "max" to { args: IntArray -> Math.max(args[0], args[1]) },
        "min" to { args: IntArray -> Math.min(args[0], args[1]) },
        "abs" to { args: IntArray -> Math.abs(args[0]) }
)

private fun interpretFunction(function: Function): Int {
    val f = functions[function.identifier.token]
            ?: throw RuntimeException("Unknown function: " + function.identifier.token)
    val args = function.arguments.map { interpret(it) }.toIntArray()
    return f(args)
}

private fun interpretNumberTerm(numberTerm: NumberTerm): Int {
    return if (numberTerm.unaryOperator == Minus) {
        -numberTerm.number.token.toInt()
    } else {
        numberTerm.number.token.toInt()
    }
}
