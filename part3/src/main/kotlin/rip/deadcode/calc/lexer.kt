package rip.deadcode.calc

import rip.deadcode.calc.Token.*
import rip.deadcode.calc.Token.Braces.ParenthesesClose
import rip.deadcode.calc.Token.Braces.ParenthesesOpen
import rip.deadcode.calc.Token.Number
import rip.deadcode.calc.Token.Operator.PrimaryOperator.Minus
import rip.deadcode.calc.Token.Operator.PrimaryOperator.Plus
import rip.deadcode.calc.Token.Operator.SecondaryOperator.Divide
import rip.deadcode.calc.Token.Operator.SecondaryOperator.Multiply

data class LexResult(
        val token: Token,
        val rest: String
)

fun lex(source: String): List<Token> {
    val result = lexSymbols(source)
            ?: lexIdentifier(source)
            ?: lexNumber(source)
            ?: lexWhitespace(source)
            ?: throw RuntimeException("Unexpected token near: " + source.substring(0, 10))

    val token = if (result.token != Whitespace) {
        listOf(result.token)
    } else {
        listOf()
    }

    return if (result.rest.isEmpty()) {
        token
    } else {
        token + lex(result.rest)
    }
}

fun lexSymbols(source: String): LexResult? {
    return when {
        source.startsWith("+") -> LexResult(Plus, source.substring(1))
        source.startsWith("-") -> LexResult(Minus, source.substring(1))
        source.startsWith("*") -> LexResult(Multiply, source.substring(1))
        source.startsWith("/") -> LexResult(Divide, source.substring(1))
        source.startsWith("(") -> LexResult(ParenthesesOpen, source.substring(1))
        source.startsWith(")") -> LexResult(ParenthesesClose, source.substring(1))
        source.startsWith(",") -> LexResult(Comma, source.substring(1))
        else -> null
    }
}

private val regIdentifier = Regex("^[a-z][a-zA-Z0-9_\\-]*")

fun lexIdentifier(source: String): LexResult? {
    val result = regIdentifier.find(source)
    return if (result != null) {
        LexResult(Identifier(result.value), source.substring(result.range.last + 1))
    } else {
        null
    }
}

private val regNumber = Regex("^[1-9][0-9]*")

fun lexNumber(source: String): LexResult? {
    val result = regNumber.find(source)
    return if (result != null) {
        LexResult(Number(result.value), source.substring(result.range.last + 1))
    } else {
        null
    }
}

fun lexWhitespace(source: String): LexResult? {
    return if (source[0].isWhitespace() || source[0] == '\r' || source[0] == '\n') {
        LexResult(Whitespace, source.substring(1))
    } else {
        null
    }
}
