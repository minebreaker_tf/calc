package rip.deadcode.calc

import rip.deadcode.calc.Expression.Function
import rip.deadcode.calc.Expression.OperativeExpression
import rip.deadcode.calc.Token.*
import rip.deadcode.calc.Token.Braces.ParenthesesClose
import rip.deadcode.calc.Token.Braces.ParenthesesOpen
import rip.deadcode.calc.Token.Number
import rip.deadcode.calc.Token.Operator.PrimaryOperator
import rip.deadcode.calc.Token.Operator.SecondaryOperator

data class ParseResult<out T>(
        val result: T,
        val unparsed: List<Token>
)

fun parse(tokens: List<Token>): Expression {
    val result = parseExpression(tokens)
    if (result.unparsed.isNotEmpty()) throw RuntimeException("Unexpected token: " + result.unparsed[0])
    return result.result
}

fun parseExpression(tokens: List<Token>): ParseResult<Expression> {

    val left = parseSecondaryExpression(tokens)
    val rest = parseRest(left.result, left.unparsed)

    return if (rest != null) {
        ParseResult(rest.result, rest.unparsed)
    } else {
        ParseResult(left.result, left.unparsed)
    }
}

private fun parseRest(left: Expression, tokens: List<Token>): ParseResult<Expression>? {

    return if (tokens.isNotEmpty() && tokens[0] is PrimaryOperator) {
        val op = tokens[0] as Operator
        val right = parseSecondaryExpression(tokens.skip(1))
        val result = OperativeExpression(left, op, right.result)
        val rest = parseRest(result, right.unparsed)

        if (rest != null) {
            ParseResult(rest.result, rest.unparsed)
        } else {
            ParseResult(result, right.unparsed)
        }
    } else {
        null
    }
}

fun parseSecondaryExpression(tokens: List<Token>): ParseResult<Expression> {

    val left = parseTerm(tokens)
    val rest = parseSecondaryRest(left.result, left.unparsed)

    return if (rest != null) {
        ParseResult(rest.result, rest.unparsed)
    } else {
        ParseResult(left.result, left.unparsed)
    }
}

private fun parseSecondaryRest(left: Expression, tokens: List<Token>): ParseResult<Expression>? {

    return if (tokens.isNotEmpty() && tokens[0] is SecondaryOperator) {
        val op = tokens[0] as Operator
        val right = parseTerm(tokens.skip(1))
        val result = OperativeExpression(left, op, right.result)
        val rest = parseSecondaryRest(result, right.unparsed)

        if (rest != null) {
            ParseResult(rest.result, rest.unparsed)
        } else {
            ParseResult(result, right.unparsed)
        }

    } else {
        ParseResult(left, tokens)
    }
}

fun parseTerm(tokens: List<Token>): ParseResult<Expression> {

    return when (tokens[0]) {
        is ParenthesesOpen -> {
            val result = parseExpression(tokens.skip(1))
            if (result.unparsed[0] != ParenthesesClose) throw RuntimeException("Unexpected token: " + result.unparsed[0])
            ParseResult(result.result, result.unparsed.skip(1))
        }
        is Token.Identifier -> parseFunction(tokens)
        is Number -> ParseResult(Expression.NumberTerm(tokens[0] as Number), tokens.skip(1))
        is Operator -> ParseResult(Expression.NumberTerm(tokens[1] as Number, tokens[0] as Operator), tokens.skip(2))
        else -> throw RuntimeException("Unexpected token: " + tokens[0])
    }
}

fun parseFunction(tokens: List<Token>): ParseResult<Function> {

    val identifier = tokens[0] as Identifier
    if (tokens[1] != ParenthesesOpen) throw RuntimeException("Unexpected token: " + tokens[1])
    val arguments = parseArgumentsWith(tokens.skip(2))
    if (arguments.unparsed[0] != ParenthesesClose) throw RuntimeException("Unexpected token: " + arguments.unparsed[0])

    return ParseResult(Function(identifier, arguments.result), arguments.unparsed.skip(1))
}

private fun parseArgumentsWith(tokens: List<Token>): ParseResult<List<Expression>> {

    val expr = parseExpression(tokens)

    return if (expr.unparsed.isNotEmpty() && expr.unparsed[0] == Comma) {
        val trailing = parseArgumentsWith(expr.unparsed.skip(1))
        ParseResult(listOf(expr.result) + trailing.result, trailing.unparsed)
    } else {
        ParseResult(listOf(expr.result), expr.unparsed)
    }
}
