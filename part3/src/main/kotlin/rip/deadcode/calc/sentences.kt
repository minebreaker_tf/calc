package rip.deadcode.calc

sealed class Expression {

    data class OperativeExpression(
            val left: Expression,
            val operator: Token.Operator,
            val right: Expression
    ) : Expression()

    data class Function(
            val identifier: Token.Identifier,
            val arguments: List<Expression>
    ) : Expression()

    data class NumberTerm(
            val number: Token.Number,
            val unaryOperator: Token.Operator? = null
    ) : Expression()
}
