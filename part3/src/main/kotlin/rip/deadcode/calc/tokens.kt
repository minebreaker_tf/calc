package rip.deadcode.calc

import com.google.common.base.MoreObjects
import java.util.*

sealed class Token(val token: String) {

    sealed class Operator(token: String) : Token(token) {

        sealed class PrimaryOperator(token: String) : Operator(token) {
            object Plus : PrimaryOperator("+")
            object Minus : PrimaryOperator("-")
        }

        sealed class SecondaryOperator(token: String) : Operator(token) {
            object Multiply : SecondaryOperator("*")
            object Divide : SecondaryOperator("/")
        }
    }

    class Identifier(token: String) : Token(token)

    object Comma : Token(",")

    sealed class Braces(token: String) : Token(token) {
        object ParenthesesOpen : Braces("(")
        object ParenthesesClose : Braces(")")
    }

    class Number(number: String) : Token(number)

    object Whitespace : Token(" ")

    override fun equals(other: Any?): Boolean {
        return other is Token && this::class == other::class && this.token == other.token
    }

    override fun hashCode(): Int {
        return Objects.hashCode(token)
    }

    override fun toString(): String {
        return MoreObjects.toStringHelper(this).add("token", token).toString()
    }
}
