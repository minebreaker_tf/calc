package rip.deadcode.calc

import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.Test

class InterpreterTest {

    @Test
    fun test1() {
        val expr = parse(lex("4 - 3 - 2 - 1"))
        println(expr)
        val result = interpret(expr)
        assertThat(result).isEqualTo(-2)
    }

    @Test
    fun test2() {
        val expr = parse(lex("3 - 2 - 1"))
        println(expr)
        val result = interpret(expr)
        assertThat(result).isEqualTo(0)
    }

    @Test
    fun test3() {
        val expr = parse(lex("3 * (14 - max(3, sqrt(4)) * (6 - 2) - 1)"))
        println(expr)
        val result = interpret(expr)
        assertThat(result).isEqualTo(3)
    }

    @Test
    fun test4() {
        val expr = parse(lex("8 - 2 * 2  - 2"))
        println(expr)
        val result = interpret(expr)
        assertThat(result).isEqualTo(2)
    }

}
