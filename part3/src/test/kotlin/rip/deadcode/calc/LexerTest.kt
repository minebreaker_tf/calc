package rip.deadcode.calc

import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.Test

internal class LexerTest {

    @Test
    fun testLex1() {
        val result = lex("1 + 2 * 3 / 4")
        assertThat(result[0].token).isEqualTo("1")
        assertThat(result[1].token).isEqualTo("+")
        assertThat(result[2].token).isEqualTo("2")
        assertThat(result[3].token).isEqualTo("*")
        assertThat(result[4].token).isEqualTo("3")
        assertThat(result[5].token).isEqualTo("/")
        assertThat(result[6].token).isEqualTo("4")
    }

    @Test
    fun testLex2() {
        val result = lex("-123-456")
        assertThat(result[0].token).isEqualTo("-")
        assertThat(result[1].token).isEqualTo("123")
        assertThat(result[2].token).isEqualTo("-")
        assertThat(result[3].token).isEqualTo("456")
    }

    @Test
    fun testLex3() {
        val result = lex("foo(123, 456)")
        assertThat(result[0].token).isEqualTo("foo")
        assertThat(result[1].token).isEqualTo("(")
        assertThat(result[2].token).isEqualTo("123")
        assertThat(result[3].token).isEqualTo(",")
        assertThat(result[4].token).isEqualTo("456")
        assertThat(result[5].token).isEqualTo(")")
    }
}
