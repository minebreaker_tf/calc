package rip.deadcode.calc

import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.Test
import rip.deadcode.calc.Expression.NumberTerm
import rip.deadcode.calc.Expression.OperativeExpression
import rip.deadcode.calc.Token.Operator.PrimaryOperator.Minus
import rip.deadcode.calc.Token.Operator.PrimaryOperator.Plus
import rip.deadcode.calc.Token.Operator.SecondaryOperator.Multiply

class ParserTest {

    @Test
    fun test1() {

        val expr = parse(lex("123")) as NumberTerm

        assertThat(expr.number.token).isEqualTo("123")
        assertThat(expr.unaryOperator).isNull()
    }

    @Test
    fun test2() {

        val expr = parse(lex("3-2+1")) as OperativeExpression
        val left = expr.left as OperativeExpression
        val right = expr.right as NumberTerm

        assertThat((left.left as NumberTerm).number.token).isEqualTo("3")
        assertThat(left.operator).isEqualTo(Minus)
        assertThat((left.right as NumberTerm).number.token).isEqualTo("2")
        assertThat(right.number.token).isEqualTo("1")
        assertThat(expr.operator).isEqualTo(Plus)
    }

    @Test
    fun test3() {

        val expr = parse(lex("(1 + 2) * 3")) as OperativeExpression
        val left = expr.left as OperativeExpression

        assertThat(expr.operator).isEqualTo(Multiply)
        assertThat(left.operator).isEqualTo(Plus)
        assertThat((left.left as NumberTerm).number.token).isEqualTo("1")
        assertThat((left.right as NumberTerm).number.token).isEqualTo("2")
        assertThat((expr.right as NumberTerm).number.token).isEqualTo("3")
    }

    @Test
    fun test4() {

        val expr = parse(lex("1 + (2 * 3)")) as OperativeExpression
        val right = expr.right as OperativeExpression

        assertThat((expr.left as NumberTerm).number.token).isEqualTo("1")
        assertThat(expr.operator).isEqualTo(Plus)
        assertThat((right.left as NumberTerm).number.token).isEqualTo("2")
        assertThat(right.operator).isEqualTo(Multiply)
        assertThat((right.right as NumberTerm).number.token).isEqualTo("3")
    }

    @Test
    fun test5() {

        val expr = parse(lex("3-2-1")) as OperativeExpression
        val left = expr.left as OperativeExpression

        assertThat(left.operator).isEqualTo(Minus)
        assertThat((left.left as NumberTerm).number.token).isEqualTo("3")
        assertThat((left.right as NumberTerm).number.token).isEqualTo("2")
        assertThat((expr.right as NumberTerm).number.token).isEqualTo("1")
    }

    @Test
    fun test6() {

        val expr = parse(lex("foo(123, 5 * (-4))")) as Expression.Function
        val arg1 = expr.arguments[0] as NumberTerm
        val arg2 = expr.arguments[1] as OperativeExpression

        assertThat(expr.identifier.token).isEqualTo("foo")
        assertThat(arg1.number.token).isEqualTo("123")
        assertThat(arg2.operator).isEqualTo(Multiply)
        assertThat((arg2.left as NumberTerm).number.token).isEqualTo("5")
        val arg21 = arg2.right as NumberTerm
        assertThat(arg21.number.token).isEqualTo("4")
        assertThat(arg21.unaryOperator).isEqualTo(Minus)
    }
}
