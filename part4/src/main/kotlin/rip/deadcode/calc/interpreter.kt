package rip.deadcode.calc

import rip.deadcode.calc.Expression.OperativeExpression
import rip.deadcode.calc.Expression.Term
import rip.deadcode.calc.Factor.Function
import rip.deadcode.calc.Statement.*
import rip.deadcode.calc.Token.Operator.PrimaryOperator.Minus
import rip.deadcode.calc.Token.Operator.PrimaryOperator.Plus
import rip.deadcode.calc.Token.Operator.SecondaryOperator.Divide
import rip.deadcode.calc.Token.Operator.SecondaryOperator.Multiply

data class Environment(
        val variables: Map<String, Int>
)

fun interpret(statements: List<Statement>, env: Environment = Environment(mapOf())) {
    interpretStatements(statements, env)
}

private tailrec fun interpretStatements(statements: List<Statement>, env: Environment = Environment(mapOf())): Environment {

    return if (statements.isNotEmpty()) {
        val newEnv = interpretStatement(statements[0], env)
        interpretStatements(statements.skip(1), newEnv)
    } else {
        env
    }
}

fun interpretStatement(stmt: Statement, env: Environment): Environment {

    return when (stmt) {
        is AssignmentStatement -> interpretAssignStatement(stmt, env)
        is EchoStatement -> interpretEchoStatement(stmt, env)
        is IfStatement -> interpretIfStatement(stmt, env)
        is WhileStatement -> interpretWhileStatement(stmt, env)
        is BlockStatement -> interpretBlockStatement(stmt, env)
    }
}

fun interpretAssignStatement(stmt: AssignmentStatement, env: Environment): Environment {

    val result = interpretExpression(stmt.expression, env)
    return env.copy(variables = env.variables.copy(stmt.name.token, result))
}

fun interpretEchoStatement(stmt: EchoStatement, env: Environment): Environment {

    val result = interpretExpression(stmt.expression, env)
    println(result)
    return env
}

fun interpretIfStatement(stmt: IfStatement, env: Environment): Environment {

    val cond = interpretExpression(stmt.condition,env)
    return if (cond != 0) {
        interpretStatement(stmt.ifTrue, env)
    } else {
        interpretStatement(stmt.ifFalse, env)
    }
}

tailrec fun interpretWhileStatement(stmt: WhileStatement, env: Environment): Environment {

    val cond = interpretExpression(stmt.condition, env)
    return if (cond != 0) {
        val result = interpretStatement(stmt.body, env)
        interpretWhileStatement(stmt, result)
    } else {
        env
    }
}

fun interpretBlockStatement(stmt: BlockStatement, env: Environment): Environment {

    return interpretStatements(stmt.body, env)
}

fun interpretExpression(expression: Expression, env: Environment): Int {

    return when (expression) {
        is OperativeExpression -> interpretOperativeExpression(expression, env)
        is Term -> interpretTerm(expression, env)
    }
}

private fun interpretOperativeExpression(expression: OperativeExpression, env: Environment): Int {
    val left = interpretExpression(expression.left, env)
    val right = interpretExpression(expression.right, env)

    return when (expression.operator) {
        Plus -> left + right
        Minus -> left - right
        Multiply -> left * right
        Divide -> left / right
    }
}

fun interpretTerm(term: Term, env: Environment): Int {

    return if (term.unaryOperator == Minus) {
        -interpretFactor(term.factor, env)
    } else {
        interpretFactor(term.factor, env)
    }
}

fun interpretFactor(factor: Factor, env: Environment): Int {

    return when (factor) {
        is Factor.Function -> interpretFunction(factor, env)
        is Factor.NumberFactor -> factor.number.token.toInt()
        is Factor.Variable -> env.variables[factor.name.token]!!
    }
}

private val functions = mapOf(
        "sqrt" to { args: IntArray -> Math.sqrt(args[0].toDouble()).toInt() },
        "max" to { args: IntArray -> Math.max(args[0], args[1]) },
        "min" to { args: IntArray -> Math.min(args[0], args[1]) },
        "abs" to { args: IntArray -> Math.abs(args[0]) }
)

private fun interpretFunction(function: Function, env: Environment): Int {
    val f = functions[function.name.token]
            ?: throw RuntimeException("Unknown function: " + function.name.token)
    val args = function.arguments.map { interpretExpression(it, env) }.toIntArray()
    return f(args)
}
