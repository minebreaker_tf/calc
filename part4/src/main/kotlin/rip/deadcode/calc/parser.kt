package rip.deadcode.calc

import rip.deadcode.calc.Expression.OperativeExpression
import rip.deadcode.calc.Expression.Term
import rip.deadcode.calc.Factor.*
import rip.deadcode.calc.Factor.Function
import rip.deadcode.calc.Statement.*
import rip.deadcode.calc.Token.*
import rip.deadcode.calc.Token.Braces.CurlyBraces.CurlyBracesClose
import rip.deadcode.calc.Token.Braces.CurlyBraces.CurlyBracesOpen
import rip.deadcode.calc.Token.Braces.Parentheses.ParenthesesClose
import rip.deadcode.calc.Token.Braces.Parentheses.ParenthesesOpen
import rip.deadcode.calc.Token.Keywords.*
import rip.deadcode.calc.Token.Number
import rip.deadcode.calc.Token.Operator.PrimaryOperator
import rip.deadcode.calc.Token.Operator.SecondaryOperator

data class ParseResult<out T>(
        val result: T,
        val unparsed: List<Token>
)

fun parse(tokens: List<Token>): List<Statement> {

    val result = parseStatement(tokens)
    return if (result.unparsed.isNotEmpty()) {
        listOf(result.result) + parse(result.unparsed)
    } else {
        listOf(result.result)
    }
}

fun parseStatement(tokens: List<Token>): ParseResult<Statement> {

    return when (tokens[0]) {
        is Identifier -> parseAssignmentStatement(tokens)
        Echo -> parseEchoStatement(tokens)
        If -> parseIfStatement(tokens)
        While -> parseWhileStatement(tokens)
        CurlyBracesOpen -> parseBlockStatement(tokens)
        else -> throw UnexpectedTokenException(tokens[0])
    }
}

fun parseAssignmentStatement(tokens: List<Token>): ParseResult<AssignmentStatement> {

    val name = tokens[0] as Identifier
    checkToken(tokens[1], Equals)
    val expr = parseExpression(tokens.skip(2))

    return ParseResult(AssignmentStatement(name, expr.result), expr.unparsed)
}

fun parseEchoStatement(tokens: List<Token>): ParseResult<EchoStatement> {

    checkToken(tokens[0], Echo)
    val expr = parseExpression(tokens.skip(1))

    return ParseResult(EchoStatement(expr.result), expr.unparsed)
}

fun parseIfStatement(tokens: List<Token>): ParseResult<IfStatement> {

    checkToken(tokens[0], If)
    val condition = parseExpression(tokens.skip(1))
    val ifTrue = parseStatement(condition.unparsed)
    checkToken(ifTrue.unparsed[0], Else)
    val ifFalse = parseStatement(ifTrue.unparsed.skip(1))

    return ParseResult(IfStatement(condition.result, ifTrue.result, ifFalse.result), ifFalse.unparsed)
}

fun parseWhileStatement(tokens: List<Token>): ParseResult<WhileStatement> {

    checkToken(tokens[0], While)
    val condition = parseExpression(tokens.skip(1))
    val body = parseStatement(condition.unparsed)

    return ParseResult(WhileStatement(condition.result, body.result), body.unparsed)
}

fun parseBlockStatement(tokens: List<Token>): ParseResult<BlockStatement> {

    checkToken(tokens[0], CurlyBracesOpen)

    fun parseStatements(tokens: List<Token>, parsed: List<Statement> = listOf()): ParseResult<List<Statement>> {
        return if (tokens.isEmpty() || tokens[0] == CurlyBracesClose) {
            ParseResult(parsed, tokens.skip(1))
        } else {
            val statement = parseStatement(tokens)
            val rest = parseStatements(statement.unparsed)
            ParseResult(listOf(statement.result) + rest.result, rest.unparsed)
        }
    }

    val result = parseStatements(tokens.skip(1))
    return ParseResult(BlockStatement(result.result), result.unparsed)
}

fun parseExpression(tokens: List<Token>): ParseResult<Expression> {

    val left = parseSecondaryExpression(tokens)
    val rest = parseRest(left.result, left.unparsed)

    return if (rest != null) {
        ParseResult(rest.result, rest.unparsed)
    } else {
        ParseResult(left.result, left.unparsed)
    }
}

private fun parseRest(left: Expression, tokens: List<Token>): ParseResult<Expression>? {

    return if (tokens.isNotEmpty() && tokens[0] is PrimaryOperator) {
        val op = tokens[0] as Operator
        val right = parseSecondaryExpression(tokens.skip(1))
        val result = OperativeExpression(left, op, right.result)
        val rest = parseRest(result, right.unparsed)

        if (rest != null) {
            ParseResult(rest.result, rest.unparsed)
        } else {
            ParseResult(result, right.unparsed)
        }
    } else {
        null
    }
}

fun parseSecondaryExpression(tokens: List<Token>): ParseResult<Expression> {

    val left = parseTerm(tokens)
    val rest = parseSecondaryRest(left.result, left.unparsed)

    return if (rest != null) {
        ParseResult(rest.result, rest.unparsed)
    } else {
        ParseResult(left.result, left.unparsed)
    }
}

private fun parseSecondaryRest(left: Expression, tokens: List<Token>): ParseResult<Expression>? {

    return if (tokens.isNotEmpty() && tokens[0] is SecondaryOperator) {
        val op = tokens[0] as Operator
        val right = parseTerm(tokens.skip(1))
        val result = OperativeExpression(left, op, right.result)
        val rest = parseSecondaryRest(result, right.unparsed)

        if (rest != null) {
            ParseResult(rest.result, rest.unparsed)
        } else {
            ParseResult(result, right.unparsed)
        }

    } else {
        ParseResult(left, tokens)
    }
}

fun parseTerm(tokens: List<Token>): ParseResult<Expression> {

    return when (tokens[0]) {
        is ParenthesesOpen -> {
            val result = parseExpression(tokens.skip(1))
            checkToken(result.unparsed[0], ParenthesesClose)
            ParseResult(result.result, result.unparsed.skip(1))
        }
        is PrimaryOperator -> {
            val result = parseFactor(tokens.skip(1))
            ParseResult(Term(result.result, tokens[0] as PrimaryOperator), result.unparsed)
        }
        else -> {
            val result = parseFactor(tokens)
            ParseResult(Term(result.result), result.unparsed)
        }
    }
}

fun parseFactor(tokens: List<Token>): ParseResult<Factor> {

    return if (tokens[0] is Identifier) {
        if (tokens.size > 1 && tokens[1] == ParenthesesOpen) {
            parseFunction(tokens)
        } else {
            ParseResult(Variable(tokens[0] as Identifier), tokens.skip(1))
        }

    } else if (tokens[0] is Number) {
        ParseResult(NumberFactor(tokens[0] as Number), tokens.skip(1))

    } else {
        throw UnexpectedTokenException(tokens[0])
    }
}

fun parseFunction(tokens: List<Token>): ParseResult<Function> {

    val identifier = tokens[0] as Identifier
    if (tokens[1] != ParenthesesOpen) throw RuntimeException("Unexpected token: " + tokens[1])
    val arguments = parseArgumentsWith(tokens.skip(2))
    if (arguments.unparsed[0] != ParenthesesClose) throw RuntimeException("Unexpected token: " + arguments.unparsed[0])

    return ParseResult(Function(identifier, arguments.result), arguments.unparsed.skip(1))
}

private fun parseArgumentsWith(tokens: List<Token>): ParseResult<List<Expression>> {

    val expr = parseExpression(tokens)

    return if (expr.unparsed.isNotEmpty() && expr.unparsed[0] == Comma) {
        val trailing = parseArgumentsWith(expr.unparsed.skip(1))
        ParseResult(listOf(expr.result) + trailing.result, trailing.unparsed)
    } else {
        ParseResult(listOf(expr.result), expr.unparsed)
    }
}
