package rip.deadcode.calc

import rip.deadcode.calc.Token.Identifier
import rip.deadcode.calc.Token.Number
import rip.deadcode.calc.Token.Operator.PrimaryOperator

sealed class Statement {

    data class AssignmentStatement(
            val name: Identifier,
            val expression: Expression
    ) : Statement()

    data class EchoStatement(
            val expression: Expression
    ) : Statement()

    data class IfStatement(
            val condition: Expression,
            val ifTrue: Statement,
            val ifFalse: Statement
    ) : Statement()

    data class WhileStatement(
            val condition: Expression,
            val body: Statement
    ) : Statement()

    data class BlockStatement(
            val body: List<Statement>
    ) : Statement()
}

sealed class Expression {

    data class OperativeExpression(
            val left: Expression,
            val operator: Token.Operator,
            val right: Expression
    ) : Expression()

    data class Term(
            val factor: Factor,
            val unaryOperator: PrimaryOperator? = null
    ) : Expression()

    fun castToNumber() = ((this as Term).factor as Factor.NumberFactor).number
    fun castToVariable() = (this as Term).factor as Factor.Variable
}

sealed class Factor {

    data class Function(
            val name: Identifier,
            val arguments: List<Expression>
    ) : Factor()

    data class NumberFactor(
            val number: Number
    ) : Factor()

    data class Variable(
            val name: Identifier
    ) : Factor()
}
