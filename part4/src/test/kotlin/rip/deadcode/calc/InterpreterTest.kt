package rip.deadcode.calc

import org.junit.jupiter.api.Test

class InterpreterTest {

    @Test
    fun test1() {
        interpret(parse(lex("""
            x = 3
            y = -1
            echo x + y
        """.trimIndent())))
    }

    @Test
    fun test2() {
        interpret(parse(lex("""
            r = 1
            n = 5
            while n {
                r = r * n
                n = n - 1
            }
            echo r
        """.trimIndent())))
    }

    @Test
    fun test3() {
        interpret(parse(lex("""
            last1 = 0
            last2 = 1
            count = 10
            while count {
                echo last1

                next = last1 + last2
                last1 = last2
                last2 = next

                count = count - 1
            }
        """.trimIndent())))
    }
}
