package rip.deadcode.calc

import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.Test
import rip.deadcode.calc.Token.Keywords.*

internal class LexerTest {

    @Test
    fun test1() {
        val result = lex("1 + 2 * 3 / 4")
        assertThat(result[0].token).isEqualTo("1")
        assertThat(result[1].token).isEqualTo("+")
        assertThat(result[2].token).isEqualTo("2")
        assertThat(result[3].token).isEqualTo("*")
        assertThat(result[4].token).isEqualTo("3")
        assertThat(result[5].token).isEqualTo("/")
        assertThat(result[6].token).isEqualTo("4")
    }

    @Test
    fun test2() {
        val result = lex("-123-456")
        assertThat(result[0].token).isEqualTo("-")
        assertThat(result[1].token).isEqualTo("123")
        assertThat(result[2].token).isEqualTo("-")
        assertThat(result[3].token).isEqualTo("456")
    }

    @Test
    fun test3() {
        val result = lex("foo(123, 456)")
        assertThat(result[0].token).isEqualTo("foo")
        assertThat(result[1].token).isEqualTo("(")
        assertThat(result[2].token).isEqualTo("123")
        assertThat(result[3].token).isEqualTo(",")
        assertThat(result[4].token).isEqualTo("456")
        assertThat(result[5].token).isEqualTo(")")
    }

    @Test
    fun test4() {
        val result = lex("echo if else while")
        assertThat(result[0]).isEqualTo(Echo)
        assertThat(result[1]).isEqualTo(If)
        assertThat(result[2]).isEqualTo(Else)
        assertThat(result[3]).isEqualTo(While)
    }

    @Test
    fun test5() {
        val result = lex("0123")
        assertThat(result[0].token).isEqualTo("0123")
    }
}
