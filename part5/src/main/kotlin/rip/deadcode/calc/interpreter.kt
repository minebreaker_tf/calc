package rip.deadcode.calc

import rip.deadcode.calc.Expression.OperativeExpression
import rip.deadcode.calc.Expression.Term
import rip.deadcode.calc.Factor.*
import rip.deadcode.calc.Factor.NumberFactor.DecimalFactor
import rip.deadcode.calc.Factor.NumberFactor.FloatFactor
import rip.deadcode.calc.Statement.*
import rip.deadcode.calc.Token.Operator.PrimaryOperator.Minus
import rip.deadcode.calc.Token.Operator.PrimaryOperator.Plus
import rip.deadcode.calc.Token.Operator.SecondaryOperator.*

data class Environment(
        val variables: Map<String, Any>
)

fun interpret(statements: List<Statement>, env: Environment = Environment(mapOf())) {
    interpretStatements(statements, env)
}

private tailrec fun interpretStatements(statements: List<Statement>, env: Environment = Environment(mapOf())): Environment {

    return if (statements.isNotEmpty()) {
        val newEnv = interpretStatement(statements[0], env)
        interpretStatements(statements.skip(1), newEnv)
    } else {
        env
    }
}

fun interpretStatement(stmt: Statement, env: Environment): Environment {

    return when (stmt) {
        is AssignmentStatement -> interpretAssignStatement(stmt, env)
        is EchoStatement -> interpretEchoStatement(stmt, env)
        is IfStatement -> interpretIfStatement(stmt, env)
        is WhileStatement -> interpretWhileStatement(stmt, env)
        is BlockStatement -> interpretBlockStatement(stmt, env)
    }
}

fun interpretAssignStatement(stmt: AssignmentStatement, env: Environment): Environment {

    val result = interpretExpression(stmt.expression, env)
    return env.copy(variables = env.variables.copy(stmt.name.token, result))
}

fun interpretEchoStatement(stmt: EchoStatement, env: Environment): Environment {

    val result = interpretExpression(stmt.expression, env)
    print(result)
    return env
}

fun interpretIfStatement(stmt: IfStatement, env: Environment): Environment {

    val cond = interpretExpression(stmt.condition, env)
    return if (cond != 0) {
        interpretStatement(stmt.ifTrue, env)
    } else {
        interpretStatement(stmt.ifFalse, env)
    }
}

tailrec fun interpretWhileStatement(stmt: WhileStatement, env: Environment): Environment {

    val cond = interpretExpression(stmt.condition, env)
    return if (cond != 0) {
        val result = interpretStatement(stmt.body, env)
        interpretWhileStatement(stmt, result)
    } else {
        env
    }
}

fun interpretBlockStatement(stmt: BlockStatement, env: Environment): Environment {

    return interpretStatements(stmt.body, env)
}

fun interpretExpression(expression: Expression, env: Environment): Any {

    return when (expression) {
        is OperativeExpression -> interpretOperativeExpression(expression, env)
        is Term -> interpretTerm(expression, env)
    }
}

private fun interpretOperativeExpression(expression: OperativeExpression, env: Environment): Any {
    val left = interpretExpression(expression.left, env)
    val right = interpretExpression(expression.right, env)

    return when (expression.operator) {
        Plus -> {
            if (left is Int && right is Int) {
                left + right
            } else if (left is Double && right is Int) {
                left + right
            } else if (left is Double && right is Double) {
                left + right
            } else {
                left.toString() + right.toString()
            }
        }
        Minus -> {
            if (left is Int && right is Int) {
                left - right
            } else if (left is Double && right is Int) {
                left - right
            } else if (left is Double && right is Double) {
                left - right
            } else {
                throw ClassCastException("Expected Decimal or Float but: ${left}")
            }
        }
        Multiply -> {
            if (left is Int && right is Int) {
                left * right
            } else if (left is Double && right is Int) {
                left * right
            } else if (left is Double && right is Double) {
                left * right
            } else {
                throw ClassCastException("Expected Decimal or Float but: ${left}")
            }
        }
        Divide -> {
            if (left is Int && right is Int) {
                left / right
            } else if (left is Double && right is Int) {
                left / right
            } else if (left is Double && right is Double) {
                left / right
            } else {
                throw ClassCastException("Expected Decimal or Float but: ${left}")
            }
        }
        Modulo -> {
            if (left is Int && right is Int) {
                left % right
            } else if (left is Double && right is Int) {
                left % right
            } else if (left is Double && right is Double) {
                left % right
            } else {
                throw ClassCastException("Expected Decimal or Float but: ${left}")
            }
        }
    }
}

fun interpretTerm(term: Term, env: Environment): Any {

    return if (term.unaryOperator == Minus) {
        val result = interpretFactor(term.factor, env)
        when (result) {
            is Int -> -result
            is Double -> -result
            else -> throw RuntimeException("Expected Decimal or Number but: ${result}")
        }
    } else {
        interpretFactor(term.factor, env)
    }
}

fun interpretFactor(factor: Factor, env: Environment): Any {

    return when (factor) {
        is Factor.FunctionFactor -> interpretFunction(factor, env)
        is DecimalFactor -> factor.decimal.token.toInt()
        is FloatFactor -> factor.float.token.toDouble()
        is StringFactor -> replaceStringLiterals(factor.token.value)
        is VariableFactor -> env.variables[factor.name.token]!!
    }
}

private fun replaceStringLiterals(str: String): String =
        str.replace("\\n", "\n").replace("\\t", "\t")

private val functions = mapOf(
        "sqrt" to { args: Array<Any> -> Math.sqrt(args[0].toString().toInt().toDouble()).toInt() },
        "max" to { args: Array<Any> -> Math.max(args[0].toString().toInt(), args[1].toString().toInt()) },
        "min" to { args: Array<Any> -> Math.min(args[0].toString().toInt(), args[1].toString().toInt()) },
        "abs" to { args: Array<Any> -> Math.abs(args[0].toString().toInt()) }
)

private fun interpretFunction(function: FunctionFactor, env: Environment): Any {
    val f = functions[function.name.token]
            ?: throw RuntimeException("Unknown function: " + function.name.token)
    val args = function.arguments.map { interpretExpression(it, env) }.toTypedArray()
    return f(args)
}
