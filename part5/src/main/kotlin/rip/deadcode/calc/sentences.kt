package rip.deadcode.calc

import rip.deadcode.calc.Token.Identifier
import rip.deadcode.calc.Token.NumberToken.DecimalToken
import rip.deadcode.calc.Token.NumberToken.FloatToken
import rip.deadcode.calc.Token.Operator.PrimaryOperator
import rip.deadcode.calc.Token.StringToken

sealed class Statement {

    data class AssignmentStatement(
            val name: Identifier,
            val expression: Expression
    ) : Statement()

    data class EchoStatement(
            val expression: Expression
    ) : Statement()

    data class IfStatement(
            val condition: Expression,
            val ifTrue: Statement,
            val ifFalse: Statement
    ) : Statement()

    data class WhileStatement(
            val condition: Expression,
            val body: Statement
    ) : Statement()

    data class BlockStatement(
            val body: List<Statement>
    ) : Statement()
}

sealed class Expression {

    data class OperativeExpression(
            val left: Expression,
            val operator: Token.Operator,
            val right: Expression
    ) : Expression()

    data class Term(
            val factor: Factor,
            val unaryOperator: PrimaryOperator? = null
    ) : Expression()

    fun castToDecimal() = ((this as Term).factor as Factor.NumberFactor.DecimalFactor).decimal
    fun castToFloat() = ((this as Term).factor as Factor.NumberFactor.FloatFactor).float
    fun castToString() = ((this as Term).factor as Factor.StringFactor).token

    fun castToVariable() = (this as Term).factor as Factor.VariableFactor
}

sealed class Factor {

    data class FunctionFactor(
            val name: Identifier,
            val arguments: List<Expression>
    ) : Factor()

    sealed class NumberFactor : Factor() {

        data class DecimalFactor(
                val decimal: DecimalToken
        ) : NumberFactor()

        data class FloatFactor(
                val float: FloatToken
        ) : NumberFactor()
    }

    data class StringFactor(
            val token: StringToken
    ) : Factor()

    data class VariableFactor(
            val name: Identifier
    ) : Factor()
}
