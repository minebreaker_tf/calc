package rip.deadcode.calc

import com.google.common.base.MoreObjects
import java.util.*

sealed class Token(val token: String) {

    sealed class Operator(token: String) : Token(token) {

        sealed class PrimaryOperator(token: String) : Operator(token) {
            object Plus : PrimaryOperator("+")
            object Minus : PrimaryOperator("-")
        }

        sealed class SecondaryOperator(token: String) : Operator(token) {
            object Multiply : SecondaryOperator("*")
            object Divide : SecondaryOperator("/")
            object Modulo : SecondaryOperator("%")
        }
    }

    class Identifier(token: String) : Token(token)

    object Comma : Token(",")
    object Equals : Token("=")

    sealed class Braces(token: String) : Token(token) {

        sealed class Parentheses(token: String) : Braces(token) {
            object ParenthesesOpen : Parentheses("(")
            object ParenthesesClose : Parentheses(")")
        }

        sealed class CurlyBraces(token: String) : Token(token) {
            object CurlyBracesOpen : CurlyBraces("(")
            object CurlyBracesClose : CurlyBraces(")")
        }
    }

    sealed class Keywords(token: String) : Token(token) {
        object Echo : Keywords("echo")
        object If : Keywords("if")
        object Else : Keywords("else")
        object While : Keywords("while")
    }

    sealed class NumberToken(token: String) : Token(token) {
        class DecimalToken(number: String) : NumberToken(number)
        class FloatToken(float: String) : NumberToken(float)
    }

    class StringToken(token: String) : Token(token) {
        val value = token.substring(1, token.lastIndex)
    }

    object Whitespace : Token(" ")

    override fun equals(other: Any?): Boolean {
        return other is Token && this::class == other::class && this.token == other.token
    }

    override fun hashCode(): Int {
        return Objects.hashCode(token)
    }

    override fun toString(): String {
        return MoreObjects.toStringHelper(this).add("token", token).toString()
    }
}
