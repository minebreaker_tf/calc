package rip.deadcode.calc

import com.google.common.base.Preconditions.checkState
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.NumberType
import rip.deadcode.calc.Expression.OperativeExpression
import rip.deadcode.calc.Expression.Term
import rip.deadcode.calc.Factor.*
import rip.deadcode.calc.Factor.NumberFactor.DecimalFactor
import rip.deadcode.calc.Factor.NumberFactor.FloatFactor
import rip.deadcode.calc.Statement.*
import rip.deadcode.calc.Token.Operator.PrimaryOperator.Minus
import rip.deadcode.calc.Token.Operator.PrimaryOperator.Plus
import rip.deadcode.calc.Token.Operator.SecondaryOperator.*
import rip.deadcode.calc.TypeClass.*

typealias TypeMap = Map<String, TypeClass>

fun checkType(statements: List<Statement>, types: TypeMap = mapOf()): TypeMap {

    return if (statements.isNotEmpty()) {
        val result = checkType(statements[0], types)
        checkType(statements.skip(1), result)
    } else {
        types
    }
}

fun checkType(statement: Statement, types: TypeMap): TypeMap {
    return when (statement) {
        is AssignmentStatement -> {
            types + (statement.name.token to inferType(statement.expression, types))
        }
        is EchoStatement -> {
            inferType(statement.expression, types)
            types
        }
        is IfStatement -> {
            inferType(statement.condition, types)
            checkType(statement.ifTrue, types)
            checkType(statement.ifFalse, types)
            types  // Do not allow leak of binding inside if
        }
        is WhileStatement -> {
            inferType(statement.condition, types)
            checkType(statement.body, types)
            types
        }
        is BlockStatement -> {
            checkType(statement.body, types)
            types
        }
    }
}

private data class FunctionType(
        val args: List<TypeClass>,
        val returnType: TypeClass
)

fun inferType(expression: Expression, types: TypeMap): TypeClass {

    return when (expression) {
        is OperativeExpression -> {
            val rightType = inferType(expression.right, types)
            val leftType = inferType(expression.left, types)
            when (expression.operator) {
                Plus -> {
                    if (rightType == DecimalType && leftType == DecimalType) {
                        DecimalType
                    } else if (rightType is NumberType && leftType is NumberType) {
                        FloatType
                    } else {
                        StringType
                    }
                }
                Minus, Multiply, Divide, Modulo -> {
                    if (rightType == DecimalType && leftType == DecimalType) {
                        DecimalType
                    } else if (rightType is NumberType && leftType is NumberType) {
                        FloatType
                    } else {
                        throw TypeCheckException(leftType, rightType)
                    }
                }
            }
        }
        is Term -> {
            if (expression.unaryOperator == null) {
                inferType(expression.factor, types)
            } else {
                val factorType = inferType(expression.factor, types)
                if (factorType == DecimalType || factorType == FloatType) {
                    factorType
                } else {
                    throw TypeCheckException(factorType)
                }
            }
        }
    }
}

private val functionTypes = mapOf(
        "sqrt" to FunctionType(listOf(DecimalType), DecimalType),
        "max" to FunctionType(listOf(DecimalType, DecimalType), DecimalType),
        "min" to FunctionType(listOf(DecimalType, DecimalType), DecimalType),
        "abs" to FunctionType(listOf(DecimalType, DecimalType), DecimalType)
)

fun inferType(factor: Factor, types: TypeMap): TypeClass {
    return when (factor) {
        is FunctionFactor -> {
            val desc = functionTypes[factor.name.token]!!
            val actualArgTypes = factor.arguments.map { inferType(it, types) }
            checkState(desc.args == actualArgTypes)
            desc.returnType
        }
        is DecimalFactor -> DecimalType
        is FloatFactor -> FloatType
        is StringFactor -> StringType
        is VariableFactor -> types[factor.name.token] ?: throw RuntimeException("Could not find the binding for '${factor.name.token}'")
    }
}
