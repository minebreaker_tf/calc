package rip.deadcode.calc

sealed class TypeClass {
    object StringType : TypeClass()
    object DecimalType : TypeClass()
    object FloatType : TypeClass()
}
