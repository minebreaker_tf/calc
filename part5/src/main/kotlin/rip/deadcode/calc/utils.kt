package rip.deadcode.calc

import com.google.common.collect.ImmutableMap
import java.util.*
import kotlin.collections.HashMap

fun <T> List<T>.skip(from: Int): List<T> =
        if (from == this.size) listOf() else this.subList(from, this.size)

fun <K, V> Map<K, V>.copy(key: K, value: V): Map<K, V> {
    val copy = HashMap(this)
    copy[key] = value
    return ImmutableMap.copyOf(copy)
}

class UnexpectedTokenException private constructor(message: String) : RuntimeException(message) {
    constructor(token: Token) : this("Unexpected token: ${token}")
}

fun checkToken(actual: Token, expected: Token): Token {
    return if (Objects.equals(actual, expected)) {
        actual
    } else {
        throw UnexpectedTokenException(actual)
    }
}

class TypeCheckException private constructor(message: String) : RuntimeException(message) {
    constructor(type1: TypeClass, type2: TypeClass) : this("Type check failed: ${type1} and ${type2}")
    constructor(type: TypeClass) : this("Type check failed: ${type}")
}
