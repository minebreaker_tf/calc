package rip.deadcode.calc

import org.junit.jupiter.api.Test

class InterpreterTest {

    @Test
    fun test1() {
        interpret(parse(lex("""
            x = 3
            y = -1
            echo x + y
        """.trimIndent())))
    }

    @Test
    fun test2() {
        interpret(parse(lex("""
            r = 3
            pi = 3.14
            result = pi * r * r
            echo result
        """.trimIndent())))
    }

    @Test
    fun factorial() {
        interpret(parse(lex("""
            result = 1
            n = 5
            while n {
                result = result * n
                n = n - 1
            }
            echo "5! = " + result
        """.trimIndent())))
    }

    @Test
    fun fibonacci() {
        interpret(parse(lex("""
            last1 = 0
            last2 = 1
            count = 10
            while count {
                echo last1 + ","

                next = last1 + last2
                last1 = last2
                last2 = next

                count = count - 1
            }
        """.trimIndent())))
    }

    @Test
    fun fizzbuzz() {
        interpret(parse(lex("""
            n = 1
            while n - 101 {
                if n % 15
                    if n % 5
                        if n % 3
                            echo n
                        else
                            echo "fizz"
                    else
                        echo "buzz"
                else
                    echo "fizzbuzz"
                echo ","
                n = n + 1
            }
        """.trimIndent())))
    }
}
