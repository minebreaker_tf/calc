package rip.deadcode.calc

import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.Test
import rip.deadcode.calc.Expression.OperativeExpression
import rip.deadcode.calc.Expression.Term
import rip.deadcode.calc.Factor.FunctionFactor
import rip.deadcode.calc.Factor.VariableFactor
import rip.deadcode.calc.Statement.*
import rip.deadcode.calc.Token.Operator.PrimaryOperator.Minus
import rip.deadcode.calc.Token.Operator.PrimaryOperator.Plus
import rip.deadcode.calc.Token.Operator.SecondaryOperator.Multiply

class ParserTest {

    @Test
    fun test1() {

        val expr = parseExpression(lex("123")).result as Term

        assertThat(expr.castToDecimal().token).isEqualTo("123")
        assertThat(expr.unaryOperator).isNull()
    }

    @Test
    fun test2() {

        val expr = parseExpression(lex("3-2+1")).result as OperativeExpression
        val left = expr.left as OperativeExpression

        assertThat(left.left.castToDecimal().token).isEqualTo("3")
        assertThat(left.operator).isEqualTo(Minus)
        assertThat(left.right.castToDecimal().token).isEqualTo("2")
        assertThat(expr.right.castToDecimal().token).isEqualTo("1")
        assertThat(expr.operator).isEqualTo(Plus)
    }

    @Test
    fun test3() {

        val expr = parseExpression(lex("(1 + 2) * 3")).result as OperativeExpression
        val left = expr.left as OperativeExpression

        assertThat(expr.operator).isEqualTo(Multiply)
        assertThat(left.operator).isEqualTo(Plus)
        assertThat(left.left.castToDecimal().token).isEqualTo("1")
        assertThat(left.right.castToDecimal().token).isEqualTo("2")
        assertThat(expr.right.castToDecimal().token).isEqualTo("3")
    }

    @Test
    fun test4() {

        val expr = parseExpression(lex("1 + (2 * 3)")).result as OperativeExpression
        val right = expr.right as OperativeExpression

        assertThat(expr.left.castToDecimal().token).isEqualTo("1")
        assertThat(expr.operator).isEqualTo(Plus)
        assertThat(right.left.castToDecimal().token).isEqualTo("2")
        assertThat(right.operator).isEqualTo(Multiply)
        assertThat(right.right.castToDecimal().token).isEqualTo("3")
    }

    @Test
    fun test5() {

        val expr = parseExpression(lex("3-2-1")).result as OperativeExpression
        val left = expr.left as OperativeExpression

        assertThat(left.operator).isEqualTo(Minus)
        assertThat(left.left.castToDecimal().token).isEqualTo("3")
        assertThat(left.right.castToDecimal().token).isEqualTo("2")
        assertThat(expr.right.castToDecimal().token).isEqualTo("1")
    }

    @Test
    fun test6() {

        val expr = (parseExpression(lex("foo(123, 5 * (-4))")).result as Term).factor as FunctionFactor
        val arg2 = expr.arguments[1] as OperativeExpression

        assertThat(expr.name.token).isEqualTo("foo")
        assertThat(expr.arguments[0].castToDecimal().token).isEqualTo("123")
        assertThat(arg2.operator).isEqualTo(Multiply)
        assertThat(arg2.left.castToDecimal().token).isEqualTo("5")
        assertThat(arg2.right.castToDecimal().token).isEqualTo("4")
        assertThat((arg2.right as Term).unaryOperator).isEqualTo(Minus)
    }

    @Test
    fun test7() {

        val expr = parse(lex("""
            x = 2
            y = -1
            echo x + y
        """.trimIndent()))

        val statement1 = expr[0] as AssignmentStatement
        assertThat(statement1.name.token).isEqualTo("x")
        assertThat(statement1.expression.castToDecimal().token).isEqualTo("2")

        val statement2 = expr[1] as AssignmentStatement
        assertThat(statement2.name.token).isEqualTo("y")
        val term2 = statement2.expression as Term
        assertThat(term2.castToDecimal().token).isEqualTo("1")
        assertThat(term2.unaryOperator).isEqualTo(Minus)

        val statement3 = expr[2] as EchoStatement
        val expr3 = statement3.expression as OperativeExpression
        assertThat(expr3.operator).isEqualTo(Plus)
        assertThat(((expr3.left as Term).factor as VariableFactor).name.token).isEqualTo("x")
        assertThat(((expr3.right as Term).factor as VariableFactor).name.token).isEqualTo("y")
    }

    @Test
    fun test8() {

        val stmt = parse(lex("""
            if 3 - 2 {
                foo = 123
                echo foo
            } else
                echo 1
            echo 2
        """.trimIndent()))

        val ifStatement = stmt[0] as IfStatement
        val cond = ifStatement.condition as OperativeExpression
        assertThat(cond.operator).isEqualTo(Minus)
        val ifTrue = ifStatement.ifTrue as BlockStatement
        val ifTrue1 = ifTrue.body[0] as AssignmentStatement
        assertThat(ifTrue1.name.token).isEqualTo("foo")
        assertThat(ifTrue1.expression.castToDecimal().token).isEqualTo("123")
        val ifTrue2 = ifTrue.body[1] as EchoStatement
        assertThat(ifTrue2.expression.castToVariable().name.token).isEqualTo("foo")
        val ifFalse = ifStatement.ifFalse as EchoStatement
        assertThat(ifFalse.expression.castToDecimal().token).isEqualTo("1")
        val statement2 = stmt[1] as EchoStatement
        assertThat(statement2.expression.castToDecimal().token).isEqualTo("2")
    }

    @Test
    fun test9() {

        val stmt = parse(lex("""
            foo = "123"
            bar = "456"
        """.trimIndent()))

        val stmt1 = stmt[0] as AssignmentStatement
        assertThat(stmt1.name.token).isEqualTo("foo")
        assertThat(stmt1.expression.castToString().value).isEqualTo("123")
        val stmt2 = stmt[1] as AssignmentStatement
        assertThat(stmt2.name.token).isEqualTo("bar")
        assertThat(stmt2.expression.castToString().value).isEqualTo("456")
    }
}
