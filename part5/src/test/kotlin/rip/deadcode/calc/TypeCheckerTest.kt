package rip.deadcode.calc

import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.Test
import rip.deadcode.izvestia.Core.expect

class TypeCheckerTest {

    @Test
    fun testCheck1() {
        checkType(parse(lex("""
            x = 3
            y = -1
            echo x + y
        """.trimIndent())))
    }

    @Test
    fun testCheck2() {
        expect {
            checkType(parse(lex("""
                x = 123
                y = "foo"
                result = x * y
            """.trimIndent())))
        }.throwsException {
            assertThat(it).isInstanceOf(TypeCheckException::class.java)
        }
    }

    @Test
    fun testCheck3() {
        expect {
            checkType(parse(lex("""
                { foo = 123 }
                echo foo
            """.trimIndent())))
        }.throwsException {
            assertThat(it).hasMessageThat().isEqualTo("Could not find the binding for 'foo'")
        }
    }
}
